#!/usr/bin/env python3

def square(x):
    return x**2

def triple(x):
    return x*3

def main():
    for i in range(1,11):
        tr = triple(i)
        sq = square(i)
        if sq > tr:
            break
        print(f"triple({i})=={tr} square({i})=={sq}")

if __name__ == "__main__":
    main()
