#!/usr/bin/env python3

import pandas as pd

def growing_municipalities(df):
    c2 = "Population change from the previous year, %"
    filtered = df[df[c2] > 0]
    return filtered.shape[0] / df.shape[0]

def main():
    df = pd.read_csv("src/municipal.tsv", index_col=0, sep="\t")
    df = df["Akaa":"Äänekoski"]

    print(f"Proportion of growing municipalities: {100 * growing_municipalities(df):.1f}%")
    return

if __name__ == "__main__":
    main()
