#!/usr/bin/env python3

import pandas as pd
import numpy as np


def map_name(s):
    print(list(map(str.capitalize, s.split())))
    if str(s).find(",") != -1:
        return " ".join(map(str.capitalize, s.split(", ")[::-1]))
    else:
        return " ".join(map(str.capitalize, s.split()))

def map_season(n):
    if n.isdigit():
        return int(n)
    else:
        return 2

def cleaning_data():
    df = pd.read_csv("src/presidents.tsv", sep="\t")
    df["Start"] = df["Start"].str.extract(r"(\d+)")
    df["President"] = df["President"].map(map_name)
    df["Vice-president"] = df["Vice-president"].map(map_name)
    df["Last"] = pd.to_numeric(df["Last"], errors="coerce")
    df["Seasons"] = df["Seasons"].map(map_season)
    df = df.astype({"President": "object", "Start": "int", "Seasons": "int", "Vice-president": "object"},
                   errors="ignore")
    return df


def main():
    print(cleaning_data())


if __name__ == "__main__":
    main()
