#!/usr/bin/env python3

import pandas as pd

days = {"ma": "Mon",
        "ti": "Tue",
        "ke": "Wed",
        "to": "Thu",
        "pe": "Fri",
        "la": "Sat",
        "su": "Sun"}

months = {
    "tammi": 1,
    "helmi": 2,
    "maalis": 3,
    "huhti": 4,
    "touko": 5,
    "kesä": 6,
    "heinä": 7,
    "elo": 8,
    "syys": 9,
    "loka": 10,
    "marras": 11,
    "joulu": 12
}


def split_date(df):
    d = df["Päivämäärä"].str.split(expand=True)
    d.columns = ["Weekday", "Day", "Month", "Year", "Hour"]

    hourmin = d["Hour"].str.split(":", expand=True)
    d["Hour"] = hourmin.iloc[:, 0]

    d["Weekday"] = d["Weekday"].map(days)
    d["Month"] = d["Month"].map(months)

    d = d.astype({"Weekday": object, "Day": int, "Month": int, "Year": int, "Hour": int})
    return d


def split_date_continues():
    df = pd.read_csv("src/Helsingin_pyorailijamaarat.csv", sep=";")
    df = df.dropna(how="all", axis=0).dropna(how="all", axis=1)

    return pd.concat([split_date(df), df.iloc[:, 1:]], axis=1)


def bicycle_timeseries():
    df = split_date_continues()
    df["Date"] = pd.to_datetime(df[["Year", "Month", "Day", "Hour"]])
    df = df.drop(columns=["Weekday", "Day", "Month", "Year", "Hour"])
    df = df.set_index("Date")
    return df


def main():
    print(bicycle_timeseries())
    return None


if __name__ == "__main__":
    main()
