#!/usr/bin/env python3
import math

import numpy as np
import matplotlib.pyplot as plt


def center(a):
    x, y = a.shape[:2]
    return (x - 1) / 2, (y - 1) / 2  # note the order: (center_y, center_x)


def radial_distance(a):
    center_y, center_x = center(a)

    return np.array([math.sqrt((x - center_x) ** 2,
                               (x - center_x) ** 2)
                     for x, y in a[:, :, 0]
                     ])


def scale(a, tmin=0.0, tmax=1.0):
    """Returns a copy of array 'a' with its values scaled to be in the range
[tmin,tmax]."""

    max = np.amax(a)
    scale = max / (tmin + tmax)
    return a * scale


def radial_mask(a):
    return 1 - scale(radial_distance(a))


def radial_fade(a):
    return a*radial_mask(a)


def main():
    img = plt.imread("src/painting.png")
    plt.subplot()
    plt.subplot()
    plt.subplot()
    plt.imshow(radial_fade(img))
    plt.show()


if __name__ == "__main__":
    main()
