#!/usr/bin/env python3

import math

def circle_area(r):
    return math.pi*r**2

def triangle_area(b,h):
    return b*h/2

def rectangle_area(b,h):
    return b*h

def main():
    # enter you solution here
    dict_shapes = {"triangle":  {"dimensions": ["base", "height"],
                                  "formula" : triangle_area},
                  "rectangle":  {"dimensions": ["base", "height"],
                                 "formula" : rectangle_area},
                  "circle":     {"dimensions": ["radius"],
                                 "formula" : circle_area}}

    while True:
        shape = input(f"Choose a shape ({', '.join(dict_shapes.keys())}): ")
        if shape == "":
            break
        if shape in dict_shapes:
            shapes_val = dict_shapes[shape]
            inputs = []
            for d in shapes_val["dimensions"]:
                inputs.append(int(input(f"Give {d} of the {shape}: ")))

            print(f"The area is {shapes_val['formula'](*inputs):6f}")
        else:
            print("Unknown shape!")

if __name__ == "__main__":
    main()
