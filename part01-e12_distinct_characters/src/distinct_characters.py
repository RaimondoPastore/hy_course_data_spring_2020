#!/usr/bin/env python3
from functools import reduce


def distinct_characters(L):
    return {x: len(set(x)) for x in L}

def main():
    print(distinct_characters(["check", "look", "try", "pop"]))

if __name__ == "__main__":
    main()
