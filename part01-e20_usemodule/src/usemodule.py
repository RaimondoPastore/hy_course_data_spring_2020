#!/usr/bin/env python3

# Don't modify the below hack
try:
    from src import triangle
except ModuleNotFoundError:
    import triangle

def main():
    # Call the functions from here
    print(triangle.__doc__)
    print(f"The hypothenuse is: {triangle.hypothenuse(3, 4)}")
    print(f"The area is: {triangle.area(3, 4)}")

if __name__ == "__main__":
    main()
