#!/usr/bin/env python3

def get_range(range):
    size = len(range)
    if size == 1 :
        return range[0]
    else:
        return range[0], range[size - 1] + 1

def detect_ranges(L):
    ranges = []
    curr_range = []

    for curr_val in sorted(L):
        size = len(curr_range)
        if size == 0:
            curr_range.append(curr_val)
        else:
            if curr_range[size - 1] + 1 == curr_val:
                curr_range.append(curr_val)
            else:
                ranges.append(get_range(curr_range))
                curr_range = [curr_val]

    ranges.append(get_range(curr_range))
    return ranges

def main():
    L = [2, 5, 4, 8, 12, 6, 7, 10, 13]
    result = detect_ranges(L)
    print(L)
    print(result)

if __name__ == "__main__":
    main()
