#!/usr/bin/env python3

from collections import Counter
import urllib.request
from lxml import etree

import numpy as np
import pandas as pd

from sklearn.naive_bayes import MultinomialNB
from sklearn.model_selection import cross_val_score
from sklearn import model_selection

alphabet = "abcdefghijklmnopqrstuvwxyzäö-"
alphabet_set = set(alphabet)


# Returns a list of Finnish words
def load_finnish():
    finnish_url = "https://www.cs.helsinki.fi/u/jttoivon/dap/data/kotus-sanalista_v1/kotus-sanalista_v1.xml"
    filename = "src/kotus-sanalista_v1.xml"
    load_from_net = False
    if load_from_net:
        with urllib.request.urlopen(finnish_url) as data:
            lines = []
            for line in data:
                lines.append(line.decode('utf-8'))
        doc = "".join(lines)
    else:
        with open(filename, "rb") as data:
            doc = data.read()
    tree = etree.XML(doc)
    s_elements = tree.xpath('/kotus-sanalista/st/s')
    return list(map(lambda s: s.text, s_elements))


def load_english():
    with open("src/words", encoding="utf-8") as data:
        lines = map(lambda s: s.rstrip(), data.readlines())
    return lines


def get_map_letter(s):
    d = {}
    for l in s:
        if l not in d:
            d[l] = 0
        d[l] += 1
    return d


def get_features(a):
    df = pd.DataFrame([get_map_letter(word) for word in a],
                      columns=list(alphabet)).fillna(0)
    return df.to_numpy()


def contains_valid_chars(s):
    for l in s:
        if l not in alphabet_set:
            return False
    return True


def filter_words(words):
    return [word.lower() for word in words if contains_valid_chars(word.lower())]


def get_features_and_labels():
    finnish = filter_words(load_finnish())
    english = filter_words([x for x in load_english() if not x[0].isupper()])
    return get_features(finnish + english), np.concatenate([np.zeros(len(finnish)), np.ones(len(english))])


def word_classification():
    X, y = get_features_and_labels()
    fold = model_selection.KFold(n_splits=5, shuffle=True, random_state=0)
    return cross_val_score(MultinomialNB(), X, y, cv=fold)


def main():
    print("Accuracy scores are:", word_classification())


if __name__ == "__main__":
    main()
