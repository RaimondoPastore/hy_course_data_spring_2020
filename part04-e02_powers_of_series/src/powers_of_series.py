#!/usr/bin/env python3
from functools import reduce

import pandas as pd


def powers_of_series(s, k):
    columns = range(1, k + 1)
    return pd.DataFrame({c: s.pow(c) for c in columns})


def main():
    print(powers_of_series(pd.Series([1, 2, 3, 4], index=list("abcd")), 4))


if __name__ == "__main__":
    main()
