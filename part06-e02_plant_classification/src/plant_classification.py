#!/usr/bin/env python3

from sklearn.datasets import load_iris
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn import naive_bayes
from sklearn import metrics
from sklearn.naive_bayes import GaussianNB


def plant_classification():
    iris = load_iris()
    X, y = iris.data, iris.target
    X_train, X_test, y_train, y_test = train_test_split(X,y, test_size=0.2, random_state=0)
    model = GaussianNB()
    model.fit(X_train, y_train);

    y_fitted = model.predict(X_test)
    acc = accuracy_score(y_test, y_fitted)
    return acc

def main():
    print(f"Accuracy is {plant_classification()}")

if __name__ == "__main__":
    main()
