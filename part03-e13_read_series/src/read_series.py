#!/usr/bin/env python3

import pandas as pd


def read_series():
    index = []
    vals = []
    while True:
        line = input()
        if not line: break
        (i, v) = line.split()
        index.append(i)
        vals.append(v)

    return pd.Series(vals, index=index)


def main():
    print(read_series())


if __name__ == "__main__":
    main()
