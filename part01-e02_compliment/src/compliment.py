
#!/usr/bin/env python3


def main():
    # Enter you solution here
    country_in = input("What country are you from? ")
    print(f"I have heard that {country_in} is a beautiful country.")

if __name__ == "__main__":
    main()
