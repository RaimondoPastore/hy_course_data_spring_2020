#!/usr/bin/env python3
import numpy as np
import pandas as pd


def last_week():
    df = pd.read_csv("src/UK-top40-1964-1-2.tsv", sep="\t")

    df.loc[(df.LW == "New") | (df.LW == "Re")] = np.nan
    df.loc[(df.Pos == "New") | (df.Pos == "Re")] = np.nan
    df = df.astype({"Pos": "float", "LW": "float"})

    df.loc[(df.Pos == df["Peak Pos"]) & (df.Pos < df.LW), "Peak Pos"] = np.nan

    df.Pos = df.LW
    df["LW"] = pd.np.nan
    df.WoC = df.WoC - 1

    df = df.sort_values(by=["Pos"])

    for i in range(40):
        if pd.isnull(df.Pos[i]):
            df.Pos[i] = i+1
            print(i)

    df = df.sort_values(by=["Pos"])

    df.to_csv("test2.csv")
    return df


def main():
    df = last_week()
    print("Shape: {}, {}".format(*df.shape))
    print("dtypes:", df.dtypes)
    print(df)


if __name__ == "__main__":
    main()
