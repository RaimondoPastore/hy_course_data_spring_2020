#!/usr/bin/env python3

import gzip
import math

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import make_pipeline


def read_fraction_file(file_name, fraction):
    num_lines = sum(1 for line in gzip.open(file_name))
    size = num_lines * fraction
    part = []
    with gzip.open(file_name, 'r') as f:
        for i, line in enumerate(f):
            if i >= size: break
            part.append(str(line))

    return part


def spam_detection(random_state=0, fraction=0.1):
    ham = read_fraction_file('src/ham.txt.gz', fraction)
    spam = read_fraction_file('src/spam.txt.gz', fraction)

    X = CountVectorizer().fit_transform(ham + spam)
    y = [0 for x in ham] + [1 for x in spam]
    X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.75, random_state=random_state)

    model = MultinomialNB()
    model.fit(X_train, y_train)
    labels_fitted = model.predict(X_test)
    acc = accuracy_score(labels_fitted, y_test)
    return acc, len(y_test), (y_test != labels_fitted).sum()


def main():
    accuracy, total, misclassified = spam_detection()
    print("Accuracy score:", accuracy)
    print(f"{misclassified} messages miclassified out of {total}")


if __name__ == "__main__":
    main()
