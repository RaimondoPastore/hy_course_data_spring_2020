#!/usr/bin/env python3

import pandas as pd
from sklearn.linear_model import LinearRegression


def mystery_data():
    df = pd.read_csv("src/mystery_data.tsv", sep="\t")
    model = LinearRegression(fit_intercept=False)
    Y = df.Y.to_numpy()
    X = df[["X1", "X2", "X3", "X4", "X5"]].to_numpy()
    return model.fit(X, Y).coef_


def main():
    [X1, X2, X3, X4, X5] = mystery_data()

    # print the coefficients here

    print("Coefficient of X1 is ", X1)
    print("Coefficient of X2 is ", X2)
    print("Coefficient of X3 is ", X3)
    print("Coefficient of X4 is ", X4)
    print("Coefficient of X5 is ", X5)


if __name__ == "__main__":
    main()
