#!/usr/bin/env python3

import sys


def file_count(filename):
    linecount, wordcount, charactercount = 0, 0, 0

    with open(filename, "r") as f:
        for line in f:
            linecount += 1
            wordcount += len(line.split())
            charactercount += len(line)

    return linecount, wordcount, charactercount


def main():
    for f in sys.argv[1:]:
        linecount, wordcount, charactercount = file_count(f)
        print(f"{linecount}\t{wordcount}\t{charactercount}\t{f}")



if __name__ == "__main__":
    main()
