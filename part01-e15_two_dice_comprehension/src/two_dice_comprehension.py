#!/usr/bin/env python3

def main():
    list = (f"({x},{y})" for x in range(1,7)
                         for y in range(1,7)
                         if x + y == 5)

    for s in list:
        print(s)

if __name__ == "__main__":
    main()
