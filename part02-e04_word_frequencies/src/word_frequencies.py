#!/usr/bin/env python3
import string


def word_frequencies(filename):
    result = {}
    with open(filename, "r") as f:
        for line in f:
            L = [x.strip(string.punctuation) for x in line.split()]
            for word in L:
                if word in result:
                    result[word] += 1
                else:
                    result[word] = 1
    return result


def main():
    for k, v in word_frequencies("alice.txt").items():
        print(f"{k}\t{v}")


if __name__ == "__main__":
    main()
