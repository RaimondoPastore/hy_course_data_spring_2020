#!/usr/bin/env python3
import math
import sys


def summary(filename):
    L = []
    with open(filename, "r") as f:
        for line in f:
            try:
                L.append(float(line))
            except ValueError:
                pass

    total = sum(L)
    average = total / (len(L))
    std = math.sqrt(sum([(x - average) ** 2 for x in L]) / (len(L) - 1))

    return total, average, std


def main():
    for f in sys.argv[1:]:
        total, average, std = summary(f)
        print(f"File: {f} Sum: {total:.6f} Average: {average:.6f} Stddev: {std:.6f}")


if __name__ == "__main__":
    main()
