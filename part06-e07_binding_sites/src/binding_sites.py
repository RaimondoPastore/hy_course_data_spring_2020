#!/usr/bin/env python3

import pandas as pd
import numpy as np
import scipy
from sklearn.cluster import AgglomerativeClustering
from sklearn.metrics import accuracy_score
from sklearn.metrics import pairwise_distances

from matplotlib import pyplot as plt

import seaborn as sns

sns.set(color_codes=True)
import scipy.spatial as sp
import scipy.cluster.hierarchy as hc

map_nucleotide = {"A": 0, "C": 1, "G": 2, "T": 3}


def toint(x):
    list = [map_nucleotide[l] for l in x]
    return list if len(list) > 1 else list[0]


def get_features_and_labels2(filename):
    df = pd.read_csv(filename, sep="\t")
    return df['X'].apply(toint).to_numpy(), df['y'].to_numpy()


def get_features_and_labels(filename):
    df = pd.read_csv(filename, sep='\t')
    return  np.matrix(df['X'].apply(toint).tolist()), np.array(df['y'])


def plot(distances, method='average', affinity='euclidean'):
    mylinkage = hc.linkage(sp.distance.squareform(distances), method=method)
    g = sns.clustermap(distances, row_linkage=mylinkage, col_linkage=mylinkage)
    g.fig.suptitle(f"Hierarchical clustering using {method} linkage and {affinity} affinity")
    plt.show()


def find_permutation(n_clusters, real_labels, labels):
    permutation = []
    for i in range(n_clusters):
        idx = labels == i
        # Choose the most common label among data points in the cluster
        new_label = scipy.stats.mode(real_labels[idx])[0][0]
        permutation.append(new_label)
    return permutation


def cluster_euclidean(filename):
    X, y = get_features_and_labels(filename)
    n_clusters = 2
    clustering = AgglomerativeClustering(linkage='average', affinity='euclidean', n_clusters=n_clusters)
    clustering.fit_predict(X, y)
    permutation = find_permutation(n_clusters, y, clustering.labels_)
    return  accuracy_score(y, [permutation[label] for label in clustering.labels_])



def cluster_hamming(filename):
    X, y = get_features_and_labels(filename)
    n_clusters = 2
    clustering = AgglomerativeClustering(linkage='average', affinity='precomputed', n_clusters=n_clusters)
    distance_matrix = pairwise_distances(X, metric='hamming')
    clustering.fit_predict(distance_matrix, y)
    permutation = find_permutation(n_clusters, y, clustering.labels_)
    return accuracy_score(y, [permutation[label] for label in clustering.labels_])



def main():
    print("Accuracy score with Euclidean affinity is", cluster_euclidean("src/data.seq"))
    print("Accuracy score with Hamming affinity is", cluster_hamming("src/data.seq"))


if __name__ == "__main__":
    main()
