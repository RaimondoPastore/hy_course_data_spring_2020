#!/usr/bin/env python3
from functools import reduce

import numpy as np


def matrix_power(a, n):
    dim = a.shape[0]
    A = a if n > 0 else (np.linalg.inv(a) if n < 0 else np.eye(dim))
    return reduce(lambda acc, x: acc @ x, (A for _ in range(abs(n or 1))))


def main():
    a = np.array([[1, 2], [3, 4]])
    print(matrix_power(a, 1))
    return


if __name__ == "__main__":
    main()
