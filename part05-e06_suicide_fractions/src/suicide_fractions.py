#!/usr/bin/env python3

import pandas as pd

def mean_cond(x):
    a = x.copy()
    a = a[(a.suicides_no.notna()) | (a.population.notna())]
    fraction_sum = a.suicides_no / a.population
    return fraction_sum.mean()


def suicide_fractions():
    df = pd.read_csv("src/who_suicide_statistics.csv")
    return df.groupby("country").\
        apply(mean_cond)

def suicide_fractions_v2():
    df = pd.read_csv("src/who_suicide_statistics.csv")
    df["Suicide fraction"] = df["suicides_no"] / df["population"]
    result = df.groupby("country").mean()
    return result["Suicide fraction"]

def main():
    print(suicide_fractions())

if __name__ == "__main__":
    main()
