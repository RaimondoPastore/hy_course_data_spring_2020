#!/usr/bin/env python3

import pandas as pd
import matplotlib.pyplot as plt

days = {"ma": 1,
        "ti": 2,
        "ke": 3,
        "to": 4,
        "pe": 5,
        "la": 6,
        "su": 7}

months = {
    "tammi": 1,
    "helmi": 2,
    "maalis": 3,
    "huhti": 4,
    "touko": 5,
    "kesä": 6,
    "heinä": 7,
    "elo": 8,
    "syys": 9,
    "loka": 10,
    "marras": 11,
    "joulu": 12
}


def split_date(df):
    d = df["Päivämäärä"].str.split(expand=True)
    d.columns = ["Weekday", "Day", "Month", "Year", "Hour"]

    hourmin = d["Hour"].str.split(":", expand=True)
    d["Hour"] = hourmin.iloc[:, 0]

    d["Weekday"] = d["Weekday"].map(days)
    d["Month"] = d["Month"].map(months)

    d = d.astype({"Weekday": object, "Day": int, "Month": int, "Year": int, "Hour": int})
    return d


def split_date_continues():
    df = pd.read_csv("src/Helsingin_pyorailijamaarat.csv", sep=";")
    df = df.dropna(how="all", axis=0).dropna(how="all", axis=1)

    return pd.concat([split_date(df), df.iloc[:, 1:]], axis=1)


def bicycle_timeseries():
    df = split_date_continues()
    df["Date"] = pd.to_datetime(df[["Year", "Month", "Day", "Hour"]])
    df = df.drop(columns=["Day", "Month", "Year", "Hour"])
    df = df.set_index("Date")
    return df


def commute():
    df = bicycle_timeseries()
    august = df["2017-08-01" : "2017-08-31"].set_index("Weekday").groupby("Weekday").apply(sum)
    return august


def main():
    res = commute()
    print(res.index)
    res.plot()
    # weekdays = "x mon tue wed thu fri sat sun".title().split()
    # plt.gca().set_xticklabels(weekdays)
    plt.show()


if __name__ == "__main__":
    main()
