#!/usr/bin/env python3

class Prepend(object):

    def __init__(self, param):
        self.start = param

    def write(self, param):
        print(f"{self.start}{param}")


def main():
    p = Prepend("+++ ")
    p.write("Hello");


if __name__ == "__main__":
    main()
