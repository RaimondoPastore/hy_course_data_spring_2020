#!/usr/bin/env python3

import pandas as pd

def best_record_company():
    df = pd.read_csv("src/UK-top40-1964-1-2.tsv", sep="\t")
    max_pub = df.groupby("Publisher").WoC.sum().idxmax()
    return df.loc[df.Publisher  == max_pub]

def main():
    print(best_record_company())
    

if __name__ == "__main__":
    main()
