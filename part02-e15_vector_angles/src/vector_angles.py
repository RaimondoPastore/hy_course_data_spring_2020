#!/usr/bin/env python3

import numpy as np
import scipy.linalg


def vector_angles(X, Y):
    XY = (X * Y).sum(axis=1)
    XX = (X * X).sum(axis=1)
    YY = (Y * Y).sum(axis=1)

    cos = XY / (np.sqrt(XX) * np.sqrt(YY))
    rad = np.arccos(np.clip(cos, -1, 1))
    return np.degrees(rad)


def main():
    A = np.array([[0, 0, 1], [-1, 1, 0]])
    B = np.array([[0, 1, 0], [1, 1, 0]])
    print(vector_angles(A, B))


if __name__ == "__main__":
    main()
