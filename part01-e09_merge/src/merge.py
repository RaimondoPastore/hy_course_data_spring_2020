#!/usr/bin/env python3
import heapq


def merge(L1, L2):
    s1, s2 = len(L1), len(L2)
    i,j = 0,0
    merged_list = []

    while i != s1 and j != s2:
        if L1[i] <= L2[j]:
            merged_list.append(L1[i])
            i += 1
        else:
            merged_list.append(L2[j])
            j += 1

    merged_list += (L1[i:] + L2[j:])
    return merged_list
    # The solution could be reach also with heapq.merge
    # return list(heapq.merge(L1,L2))

def main():
    L1 = [1, 5, 9, 12]
    L2 = [2, 6, 10]
    print(merge(L1, L2))

if __name__ == "__main__":
    main()
