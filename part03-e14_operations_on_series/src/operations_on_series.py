#!/usr/bin/env python3

import pandas as pd


def create_series(L1, L2):
    index = ["a", "b", "c"]
    return pd.Series(L1, index=index), pd.Series(L2, index=index)


def modify_series(s1, s2):
    s1["d"] = s2["b"]
    del s2["b"]
    return s1, s2


def main():
    s1, s2 = create_series([1,2,3], [4,5,6])
    s1, s2 = modify_series(s1, s2)
    print(s1,"\n", s2)
    print(s1+s2)
    return


if __name__ == "__main__":
    main()
