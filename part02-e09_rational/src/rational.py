#!/usr/bin/env python3

class Rational(object):
    # + - * / < > ==

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __gt__(self, obj):
        return self.x / self.y > obj.x / obj.y

    def __lt__(self, obj):
        return self.x / self.y < obj.x / obj.y

    def __eq__(self, obj):
        return self.x / self.y == obj.x / obj.y

    def __mul__(self, obj):
        return Rational(self.x * obj.x, self.y * obj.y)

    def __str__(self):
        return f"{self.x}/{self.y}"

    def __truediv__(self, obj):
        return Rational(self.x * obj.y, self.y * obj.x)

    def __add__(self, obj):
        return Rational(((self.x * obj.y) + (obj.x * self.y)), (self.y * obj.y))

    def __sub__(self, obj):
        return Rational(((self.x * obj.y) - (obj.x * self.y)), (self.y * obj.y))


def main():
    r1 = Rational(1, 4)
    r2 = Rational(2, 3)
    print(r1)
    print(r2)
    print(r1 * r2)
    print(r1 / r2)
    print(r1 + r2)
    print(r1 - r2)
    print(Rational(1, 2) == Rational(2, 4))
    print(Rational(1, 2) > Rational(2, 4))
    print(Rational(1, 2) < Rational(2, 4))


if __name__ == "__main__":
    main()
