#!/usr/bin/env python3
import string

def main():
    range_10 = range(1, 11)
    for i in range_10:
        row = [f"{i*j:3d}" for j in range_10]
        print(" ".join(row))

if __name__ == "__main__":
    main()
