#!/usr/bin/env python3

import pandas as pd
import matplotlib.pyplot as plt


days = {"ma": "Mon",
        "ti": "Tue",
        "ke": "Wed",
        "to": "Thu",
        "pe": "Fri",
        "la": "Sat",
        "su": "Sun"}

months = {
    "tammi": 1,
    "helmi": 2,
    "maalis": 3,
    "huhti": 4,
    "touko": 5,
    "kesä": 6,
    "heinä": 7,
    "elo": 8,
    "syys": 9,
    "loka": 10,
    "marras": 11,
    "joulu": 12
}

def split_date(df):
    d = df["Päivämäärä"].str.split(expand=True)
    d.columns = ["Weekday", "Day", "Month", "Year", "Hour"]

    hourmin = d["Hour"].str.split(":", expand=True)
    d["Hour"] = hourmin.iloc[:, 0]

    d["Weekday"] = d["Weekday"].map(days)
    d["Month"] = d["Month"].map(months)

    d = d.astype({"Weekday": object, "Day": int, "Month": int, "Year": int, "Hour": int})
    return d


def split_date_continues():
    df = pd.read_csv("src/Helsingin_pyorailijamaarat.csv", sep=";")
    df = df.dropna(how="all", axis=0).dropna(how="all", axis=1)

    return pd.concat([split_date(df), df.iloc[:, 1:]], axis=1)


def cyclists_per_day():
    df = split_date_continues().drop(columns=["Hour", "Weekday"])
    return df.groupby(["Day", "Month", "Year"]).sum()
    
def main():
    df = cyclists_per_day()
    august = df.loc[
        (df.index.get_level_values("Year") == 2017) &
        (df.index.get_level_values("Month") == 8)
    ]

    print(august)
    august.plot()
    plt.show()

if __name__ == "__main__":
    main()
