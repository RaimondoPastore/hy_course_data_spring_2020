#!/usr/bin/env python3

import pandas as pd
from sklearn.linear_model import LinearRegression

days = {"ma": "Mon",
        "ti": "Tue",
        "ke": "Wed",
        "to": "Thu",
        "pe": "Fri",
        "la": "Sat",
        "su": "Sun"}

months = {
    "tammi": 1,
    "helmi": 2,
    "maalis": 3,
    "huhti": 4,
    "touko": 5,
    "kesä": 6,
    "heinä": 7,
    "elo": 8,
    "syys": 9,
    "loka": 10,
    "marras": 11,
    "joulu": 12
}


def split_date(df):
    d = df["Päivämäärä"].str.split(expand=True)
    d.columns = ["Weekday", "d", "m", "Year", "Hour"]

    hourmin = d["Hour"].str.split(":", expand=True)
    d["Hour"] = hourmin.iloc[:, 0]

    d["Weekday"] = d["Weekday"].map(days)
    d["m"] = d["m"].map(months)

    d = d.astype({"Weekday": object, "d": int, "m": int, "Year": int, "Hour": int})
    return d


def split_date_continues():
    df = pd.read_csv("src/Helsingin_pyorailijamaarat.csv", sep=";")
    df = df.dropna(how="all", axis=0).dropna(how="all", axis=1)

    return pd.concat([split_date(df), df.iloc[:, 1:]], axis=1)


def cycling_weather():
    df_c = split_date_continues()
    df_w = pd.read_csv("src/kumpula-weather-2017.csv", sep=",")
    df_c_2017 = df_c[df_c.Year == 2017].groupby(["Year", "m", "d"]).sum().reset_index()
    return pd.merge(df_c_2017, df_w).drop(columns=["Time", "Time zone"]).rename(
        columns={"d": "Day", "m": "Month"}).ffill() # ffill() - forward fill to fill the missing values


def cycling_weather_continues(station):
    df = cycling_weather()
    X = df[['Precipitation amount (mm)', 'Snow depth (cm)', 'Air temperature (degC)']]
    Y = df[[station]]
    model = LinearRegression().fit(X, Y)
    return model.coef_[0], model.score(X, Y)


def main():
    s = "Merikannontie"
    coef, score = cycling_weather_continues(s)
    print(f"Measuring station: {s}")
    print(f"Regression coefficient for variable 'precipitation': {coef[0]:.1f}")
    print(f"Regression coefficient for variable 'snow depth': {coef[1]:.1f}")
    print(f"Regression coefficient for variable 'temperature': {coef[2]:.1f}")
    print(f"Score: {score:.2f}")
    return


if __name__ == "__main__":
    main()
