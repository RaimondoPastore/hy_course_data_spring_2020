#!/usr/bin/env python3

import numpy as np

def diamond(n):
    a = np.eye(n, dtype=int)
    b = a[::-1]

    top = np.concatenate((b, a[:,1:]), axis=1)
    bottom = np.concatenate((a[:,:-1], b), axis=1)

    return np.concatenate((top[:-1,:], bottom), axis=0)

def main():
    print(diamond(5))

if __name__ == "__main__":
    main()
