#!/usr/bin/env python3

import pandas as pd

days = {"ma": "Mon",
        "ti": "Tue",
        "ke": "Wed",
        "to": "Thu",
        "pe": "Fri",
        "la": "Sat",
        "su": "Sun"}

months = {
    "tammi": 1,
    "helmi": 2,
    "maalis": 3,
    "huhti": 4,
    "touko": 5,
    "kesä": 6,
    "heinä": 7,
    "elo": 8,
    "syys": 9,
    "loka": 10,
    "marras": 11,
    "joulu": 12
}

def split_date(df):
    d = df["Päivämäärä"].str.split(expand=True)
    d.columns = ["Weekday", "d", "m", "Year", "Hour"]

    hourmin = d["Hour"].str.split(":", expand=True)
    d["Hour"] = hourmin.iloc[:, 0]

    d["Weekday"] = d["Weekday"].map(days)
    d["m"] = d["m"].map(months)

    d = d.astype({"Weekday": object, "d": int, "m": int, "Year": int, "Hour": int})
    return d


def split_date_continues():
    df = pd.read_csv("src/Helsingin_pyorailijamaarat.csv", sep=";")
    df = df.dropna(how="all", axis=0).dropna(how="all", axis=1)

    return pd.concat([split_date(df), df.iloc[:, 1:]], axis=1)


def cycling_weather():
    df_c =  split_date_continues()
    df_w = pd.read_csv("src/kumpula-weather-2017.csv", sep=",")

    return pd.merge(df_c, df_w).drop(columns=["Time", "Time zone"]).rename(columns={"d" :"Day", "m": "Month"})

def main():
    print(cycling_weather())

if __name__ == "__main__":
    main()
