#!/usr/bin/env python3

import pandas as pd


def swedish_and_foreigners():
    df = pd.read_csv("src/municipal.tsv", index_col=0, sep="\t")
    df = df["Akaa":"Äänekoski"]

    c1 = "Population"
    c3 = "Share of Swedish-speakers of the population, %"
    c4 = "Share of foreign citizens of the population, %"

    df = df[(df[c3] > 5) & (df[c4] > 5)]
    return df[[c1, c3, c4]]


def main():
    print(swedish_and_foreigners())
    return


if __name__ == "__main__":
    main()
