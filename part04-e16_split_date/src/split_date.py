#!/usr/bin/env python3

import pandas as pd
import numpy as np

week = {"ma": "Mon",
        "ti": "Tue",
        "ke": "Wed",
        "to": "Thu",
        "pe": "Fri",
        "la": "Sat",
        "su": "Sun"}

months = {
    "tammi": 1,
    "helmi": 2,
    "maalis": 3,
    "huhti": 4,
    "touko": 5,
    "kesä": 6,
    "heinä": 7,
    "elo": 8,
    "syys": 9,
    "loka": 10,
    "marras": 11,
    "joulu": 12
}


def map_weekday(day):
    return week[day]


def map_months(m):
    return months[m]


def split_date():
    df = pd.read_csv('src/Helsingin_pyorailijamaarat.csv', sep=";", usecols=["Päivämäärä"])
    df = df.dropna(how="all", axis=0)
    df = df.iloc[:, 0].str.split(expand=True)

    df.columns = ['Weekday', 'Day', 'Month', 'Year', 'Hour']

    df.iloc[:, 0] = df.iloc[:, 0].map(map_weekday)
    df.iloc[:, 1] = df.iloc[:, 1].astype("int")
    df.iloc[:, 2] = df.iloc[:, 2].map(map_months)
    df.iloc[:, 3] = df.iloc[:, 3].astype("int")
    df.iloc[:, -1] = df.iloc[:, -1].map(lambda x: int(x.split(":")[0]))
    return df


def main():
    print(split_date())


if __name__ == "__main__":
    main()
