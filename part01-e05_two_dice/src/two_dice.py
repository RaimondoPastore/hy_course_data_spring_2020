#!/usr/bin/env python3

def main():
    valid_sum = 5
    range_6 = range(1,7)
    for i in range_6:
        for j in range_6:
            if i + j == valid_sum:
                print(f"({i},{j})")

if __name__ == "__main__":
    main()
