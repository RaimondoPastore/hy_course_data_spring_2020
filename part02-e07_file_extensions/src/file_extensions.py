#!/usr/bin/env python3
import re


def file_extensions(filename):
    no_ext_files = []
    dict_files = {}
    with open(filename, "r") as f:
        for line in f:
            line = line.replace("\n", "")
            if line.find(".") != -1:
                (f, dot, ext) = line.rpartition(".")
                if ext in dict_files:
                    dict_files[ext].append(line)
                else:
                    dict_files[ext] = [line]
            else:
                no_ext_files.append(line)

    return no_ext_files, dict_files


def main():
    no_ext_files, dict_files = file_extensions("src/filenames.txt")
    print(f"{len(no_ext_files)} files with no extension")
    for k, v in dict_files.items():
        print(f"{k} {v}")


if __name__ == "__main__":
    main()
