#!/usr/bin/env python3

import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.metrics import r2_score
from sklearn import linear_model


def coefficient_of_determination():
    df = pd.read_csv("src/mystery_data.tsv", sep="\t")
    Y = df.Y
    X = df[["X1", "X2", "X3", "X4", "X5"]]
    arr = [X, df[["X1"]], df[["X2"]], df[["X3"]], df[["X4"]], df[["X5"]]]
    model = LinearRegression()
    return [model.fit(x, Y).score(x, Y) for x in arr]

def main():
    print(coefficient_of_determination())


if __name__ == "__main__":
    main()
