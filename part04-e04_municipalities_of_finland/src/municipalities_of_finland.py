#!/usr/bin/env python3

import pandas as pd


def municipalities_of_finland():
    df = pd.read_csv("src/municipal.tsv", index_col=0, sep="\t")
    return df["Akaa":"Äänekoski"]


def main():
    print(municipalities_of_finland().shape)


if __name__ == "__main__":
    main()
