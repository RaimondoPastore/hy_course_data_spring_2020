#!/usr/bin/env python3

import numpy as np

import matplotlib.pyplot as plt


def to_grayscale(img):
    copy = img.copy()
    return 0.2126 * copy[:, :, 0] + 0.7152 * copy[:, :, 1] + 0.0722 * copy[:, :, 2]


def to_green(img):
    copy = img.copy()
    copy[:, :, [0, 2]] = 0
    return copy


def to_red(img):
    copy = img.copy()
    copy[:, :, [1, 2]] = 0
    return copy


def to_blue(img):
    copy = img.copy()
    copy[:, :, [0, 1]] = 0
    return copy


def main():
    img = plt.imread("src/painting.png")
    plt.subplot(3, 1, 1)
    plt.imshow(to_red(img))
    plt.subplot(3, 1, 2)
    plt.imshow(to_green(img))
    plt.subplot(3, 1, 3)
    plt.imshow(to_blue(img))
    plt.show()


if __name__ == "__main__":
    main()
