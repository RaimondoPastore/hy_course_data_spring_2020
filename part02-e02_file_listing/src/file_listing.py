#!/usr/bin/env python3

import re


def file_listing(filename="src/listing.txt"):
    res = []
    with open(filename, "r") as f:
        for line in f:
            # Regex from the profsolution r".{10}\s+\d+\s+.+\s+.+\s+(\d+)\s+(...)\s+(\d+)\s+(\d\d):(\d\d)\s+(.+)"
            r = re.findall(r"([^\s]+)", line)
            size, month, day, time, filename = int(r[4]), r[5], int(r[6]), re.findall(r"[^:]+", r[7]), r[8]
            hour, minute = int(time[0]), int(time[1])
            res.append((size, month, day, hour, minute, filename))
    return res

def main():
    print(file_listing())

if __name__ == "__main__":
    main()
