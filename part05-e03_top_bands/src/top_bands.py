#!/usr/bin/env python3

import pandas as pd

def top_bands():
    top = pd.read_csv("src/UK-top40-1964-1-2.tsv", sep="\t")
    band = pd.read_csv("src/bands.tsv", sep="\t")
    band["Band"] = band["Band"].str.upper()
    return pd.merge(top, band, left_on=["Artist"], right_on=["Band"])

def main():
    print(top_bands())

if __name__ == "__main__":
    main()
