#!/usr/bin/env python3

import numpy as np


def first_half_second_half(a):
    # a1, a2 = np.split(a, 2, axis=1) suggested solution
    i = int(a.shape[1] / 2)
    first_half = a[:, :i]
    second_half = a[:, i:]
    return a[np.sum(first_half, axis=1) > np.sum(second_half, axis=1)]


def main():
    a = np.array([[1, 3, 4, 2],
                  [2, 2, 1, 2]])
    print(first_half_second_half(a))


if __name__ == "__main__":
    main()
