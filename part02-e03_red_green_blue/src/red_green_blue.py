#!/usr/bin/env python3

import re

def red_green_blue(filename="src/rgb.txt"):
    res = []
    with open(filename, "r") as f:
        for i, line in enumerate(f):
            if i:
                r = re.findall(r"\s*(\d+)\s+(\d+)\s+(\d+)\s*(.*$)", line)
                res.append("\t".join(r[0]))
    return res


def main():
    print(red_green_blue())

if __name__ == "__main__":
    main()
