#!/usr/bin/env python3

import numpy as np

def get_columns(a):
    num_rows, num_cols = a.shape
    return [a[:,c] for c in range(num_cols)]

def get_rows(a):
    num_rows, num_cols = a.shape
    return [a[r,:] for r in range(num_rows)]

def main():
    np.random.seed(0)
    a=np.random.randint(0,10, (4,4))
    print("a:", a)
    print("Rows:", get_rows(a))
    print("Columns:", get_columns(a))

if __name__ == "__main__":
    main()
