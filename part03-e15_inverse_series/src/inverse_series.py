#!/usr/bin/env python3

import pandas as pd

def inverse_series(s):
    return pd.Series(s.index, s)

def main():
    L = [1, 2, 3, 1]
    ind = list("abcd")
    s = pd.Series(L, index=ind)
    print(s)
    print(inverse_series(s))

if __name__ == "__main__":
    main()
