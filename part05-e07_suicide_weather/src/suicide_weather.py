#!/usr/bin/env python3

import pandas as pd


def suicide_fractions():
    df = pd.read_csv("src/who_suicide_statistics.csv")
    df["Suicide fraction"] = df["suicides_no"] / df["population"]
    result = df.groupby("country").mean()
    return result["Suicide fraction"]


def suicide_weather():
    columns = ["Country", "Average yearly temperature (1961–1990, degrees Celsius)", "Suicide fraction"]
    suicides = suicide_fractions()
    table = pd.read_html("src/List_of_countries_by_average_yearly_temperature.html", index_col=0, header=0)[0]
    df = pd.merge(table, suicides, left_index=True, right_index=True)
    df[columns[1]] = df[columns[1]].map(lambda s: float(s.replace("\u2212", "-")))
    corr = df[columns[1]].corr(df["Suicide fraction"], method="spearman")
    return suicides.shape[0], table.shape[0], df.shape[0], corr


def main():
    a, b, c, corr = suicide_weather()
    print(f"Suicide DataFrame has {a} rows")
    print(f"Temperature DataFrame has {b} rows")
    print(f"Common DataFrame has {c} rows")
    print(f"Spearman correlation: {corr}")
    return


if __name__ == "__main__":
    main()
