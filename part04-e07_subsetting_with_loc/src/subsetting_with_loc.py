#!/usr/bin/env python3

import pandas as pd

def subsetting_with_loc():

    df = pd.read_csv("src/municipal.tsv", index_col=0, sep="\t")

    c1 = "Population"
    c3 = "Share of Swedish-speakers of the population, %"
    c4 = "Share of foreign citizens of the population, %"

    return df.loc["Akaa":"Äänekoski", [c1, c3, c4]]

def main():
    print(subsetting_with_loc())

if __name__ == "__main__":
    main()
