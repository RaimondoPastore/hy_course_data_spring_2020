#!/usr/bin/env python3

import pandas as pd

def average_temperature():
    df = pd.read_csv("src/kumpula-weather-2017.csv")
    c = "Air temperature (degC)"
    july = df[df["m"] == 7]
    return july[c].mean()

def main():
    print("Average temperature in July: ", average_temperature())

if __name__ == "__main__":
    main()
