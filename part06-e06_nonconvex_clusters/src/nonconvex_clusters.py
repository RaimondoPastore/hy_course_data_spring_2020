#!/usr/bin/env python3

import pandas as pd
import numpy as np
from sklearn.cluster import DBSCAN
from sklearn.metrics import accuracy_score
import scipy


def find_permutation(n_clusters, real_labels, labels):

    permutation=[]

    for i in range(n_clusters):

        if labels[i] != -1:

            try:

                idx = labels == i

                new_label=scipy.stats.mode(real_labels[idx])[0][0]

                permutation.append(new_label)

            except:

                pass

    return permutation

def nonconvex_clusters_v2():
    df = pd.read_csv("src/data.tsv", sep="\t")
    X = df[["X1", "X2"]]
    y = df[["y"]]

    d = []
    for v in np.arange(0.05, 0.2, 0.05):
        model = DBSCAN(eps=v)
        model.fit(X)
        cluster = [x for x in model.labels_ if x != -1]
        outliers = [x for x in model.labels_ if x == -1]
        permutation = find_permutation(len(model.labels_), y, model.labels_)

        new_labels = [permutation[label] for label in model.labels_]  # permute the labels

        acc = accuracy_score(y, new_labels) if len(new_labels) != len(model.labels_) else np.nan
        d.append({'eps': v, "Score": acc, "Clusters": float(len(new_labels)), "Outliers": float(len(outliers))})
    return pd.DataFrame(d)

def nonconvex_clusters():
    df = pd.read_csv('src/data.tsv', sep='\t')

    X = np.array(list(zip(df['X1'], df['X2'])))

    labels = np.array(df['y'])

    new_df = pd.DataFrame()

    for x in np.arange(0.05, 0.2, 0.05):

        clustering = DBSCAN(eps=x).fit(X)

        clusters = float(len(set(clustering.labels_)) - (1 if -1 in clustering.labels_ else 0))

        outliers = float(len(X[clustering.labels_ == -1]))

        if clusters != len(np.unique(labels)):

            acc = np.nan

        else:

            permutation = find_permutation(int(clusters), labels, clustering.labels_)

            acc = (accuracy_score(labels, [permutation[label] for label in clustering.labels_]))

        data = pd.DataFrame({"eps": [x],

                             "Score":  [float('{:02.2f}'.format(acc))],

                             "Clusters": [clusters],

                             "Outliers": [outliers]})

        new_df = new_df.append(data, ignore_index=True)

    return (new_df)



def main():
    print(nonconvex_clusters())


if __name__ == "__main__":
    main()
