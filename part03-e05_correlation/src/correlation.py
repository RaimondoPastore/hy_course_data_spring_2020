#!/usr/bin/env python3

import scipy.stats
import numpy as np


def load2():
    """This loads the data from the internet. Does not work well on the TMC server."""
    import seaborn as sns
    return sns.load_dataset('iris').drop('species', axis=1).values


def load():
    import pandas as pd
    return pd.read_csv("src/iris.csv").drop('species', axis=1).values


def lengths():
    matrix = load()
    correlation, _ = scipy.stats.pearsonr(matrix[:, 0], matrix[:, 2])
    return correlation


def correlations():
    matrix = load()
    res = []
    for i in range(4):
        row = []
        for j in range(4):
            correlation = np.corrcoef(matrix[:, i], matrix[:, j])[0, 1]
            row.append(correlation)
        res.append(row)

    return np.array(res)


def main():
    print(lengths())
    print(correlations())


if __name__ == "__main__":
    main()
