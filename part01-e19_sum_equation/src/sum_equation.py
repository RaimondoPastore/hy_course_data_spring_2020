#!/usr/bin/env python3

def sum_equation(L):
    s = " + ".join(map(str,L)) if len(L) else "0"
    return f"{s} = {sum(L)}"

def main():
    print(sum_equation([1,5,7]))

if __name__ == "__main__":
    main()
