#!/usr/bin/env python3

def main():
    # Enter your solution here
    numb = 4
    for i in range(11):
        print(f"{numb} multiplied by {i} is {numb*i}")

if __name__ == "__main__":
    main()
