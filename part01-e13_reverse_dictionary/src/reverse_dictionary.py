#!/usr/bin/env python3

def reverse_dictionary(d):

    FI_EN_dict = {}
    for k, vals in d.items():
        for v in vals:
            if v in FI_EN_dict:
                FI_EN_dict[v].append(k)
            else:
                FI_EN_dict[v] = [k]

    return FI_EN_dict

def main():
    d = {'move': ['liikuttaa'], 'hide': ['piilottaa', 'salata'], 'six': ['kuusi'], 'fir': ['kuusi']}
    print(reverse_dictionary(d))

if __name__ == "__main__":
    main()
