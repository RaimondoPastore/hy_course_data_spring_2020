#!/usr/bin/env python3
from functools import reduce

import numpy as np


def most_frequent_first(a, c):
    col = a[:, c]
    d = {}
    print(np.unique(col, return_inverse=True, return_counts=True))
    for i, x in enumerate(col):
        for u in np.unique(col):
            if u == x:
                if u not in d:
                    d[u] = {"c": 0, "i": []}
                d[u]["c"] += 1
                d[u]["i"].append(i)

    sorted_by_frequency = sorted(d.items(), key=lambda x: x[1]["c"], reverse=True)

    idx = reduce(lambda a, b: a + list(b[1]["i"]), sorted_by_frequency, [])
    print("index", idx)

    return a[idx]


def main():
    n, m = 10, 10
    a = np.random.randint(0, 10, (n, m))
    print(most_frequent_first(a, -1))


if __name__ == "__main__":
    main()
