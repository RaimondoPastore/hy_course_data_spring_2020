#!/usr/bin/env python3
from functools import reduce


def interleave(*lists):
    return reduce(lambda acc, tuple: acc + list(tuple), zip(*lists), [])

def main():
    print(interleave([1, 2, 3], [20, 30, 40], ['a', 'b', 'c']))

if __name__ == "__main__":
    main()
